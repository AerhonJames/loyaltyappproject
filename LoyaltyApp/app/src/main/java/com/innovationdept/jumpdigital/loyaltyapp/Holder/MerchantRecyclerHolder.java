package com.innovationdept.jumpdigital.loyaltyapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.innovationdept.jumpdigital.loyaltyapp.R;

/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class MerchantRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public TextView merchinfo [] = new TextView[4];
    public ImageView merch_Image;
    public MerchantRecyclerHolder (View ItemView){
        super(ItemView);
        itemView.setOnClickListener(this);
        merchinfo[0] = (TextView)ItemView.findViewById(R.id.merch_name);
        merchinfo[1] = (TextView)ItemView.findViewById(R.id.merch_address);
        merchinfo[2] = (TextView)ItemView.findViewById(R.id.merch_contact_per);
        merchinfo[3] = (TextView)ItemView.findViewById(R.id.merch_contact_per_num);
        merch_Image = (ImageView)ItemView.findViewById(R.id.merch_image);
    }

    @Override
    public void onClick(View v) {
    }
}
