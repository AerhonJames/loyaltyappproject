package com.innovationdept.jumpdigital.loyaltyapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innovationdept.jumpdigital.loyaltyapp.Holder.ProductsRecyclerHolder;
import com.innovationdept.jumpdigital.loyaltyapp.Model.Products;
import com.innovationdept.jumpdigital.loyaltyapp.R;

import java.util.List;


/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class ProductsRecyclerAdapter extends RecyclerView.Adapter<ProductsRecyclerHolder> {

    private List<Products> itemsList;
    private Context context;

    public ProductsRecyclerAdapter(Context context, List<Products> itemsList) {
        this.itemsList = itemsList;
        this.context = context;
    }

    @Override
    public ProductsRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_productsview, null);
        ProductsRecyclerHolder prh = new ProductsRecyclerHolder(layoutView);
        return prh;
    }

    @Override
    public void onBindViewHolder(ProductsRecyclerHolder holder, int position) {
        holder.prodinfo[0].setText(itemsList.get(position).getProd_name());
        holder.prodinfo[1].setText(itemsList.get(position).getProd_points());
    }

    @Override
    public int getItemCount() {
        return 0;
    }


}
