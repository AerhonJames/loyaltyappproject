package com.innovationdept.jumpdigital.loyaltyapp.Holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class RewardsRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public RewardsRecyclerHolder (View ItemView){
        super(ItemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
    }
}
