package com.innovationdept.jumpdigital.loyaltyapp.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innovationdept.jumpdigital.loyaltyapp.R;

/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class MerchantFragment extends Fragment {
    private RecyclerView merch_recycler;
    public static Fragment newInstance (Context context){
        MerchantFragment MF = new MerchantFragment();
        return MF;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.merchant_fragment, parent, false);

        merch_recycler = (RecyclerView)root.findViewById(R.id.merch_recycler);

        return root;
    }
}
