package com.innovationdept.jumpdigital.loyaltyapp.Model;

/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class Rewards {
    private int rew_id;
    private int added_points;
    private double subtracted_points;
    private String rew_item;
    private String date;
    private int user_id;
    private int merch_id;

    public Rewards(){

    }

    public Rewards(int rew_id, int added_points, double subtracted_points, String rew_item, String date, int user_id, int merch_id) {
        this.rew_id = rew_id;
        this.added_points = added_points;
        this.subtracted_points = subtracted_points;
        this.rew_item = rew_item;
        this.date = date;
        this.user_id = user_id;
        this.merch_id = merch_id;
    }

    public int getRew_id() {
        return rew_id;
    }

    public void setRew_id(int rew_id) {
        this.rew_id = rew_id;
    }

    public int getAdded_points() {
        return added_points;
    }

    public void setAdded_points(int added_points) {
        this.added_points = added_points;
    }

    public double getSubtracted_points() {
        return subtracted_points;
    }

    public void setSubtracted_points(double subtracted_points) {
        this.subtracted_points = subtracted_points;
    }

    public String getRew_item() {
        return rew_item;
    }

    public void setRew_item(String rew_item) {
        this.rew_item = rew_item;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getMerch_id() {
        return merch_id;
    }

    public void setMerch_id(int merch_id) {
        this.merch_id = merch_id;
    }

    @Override
    public String toString() {
        return "Rewards{" +
                "rew_id=" + rew_id +
                ", added_points=" + added_points +
                ", subtracted_points=" + subtracted_points +
                ", rew_item='" + rew_item + '\'' +
                ", date='" + date + '\'' +
                ", user_id=" + user_id +
                ", merch_id=" + merch_id +
                '}';
    }
}
