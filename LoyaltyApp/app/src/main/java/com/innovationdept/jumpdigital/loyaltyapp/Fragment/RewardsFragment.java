package com.innovationdept.jumpdigital.loyaltyapp.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innovationdept.jumpdigital.loyaltyapp.R;

/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class RewardsFragment extends Fragment {
    public static Fragment newInstance (Context context){
        RewardsFragment RF = new RewardsFragment();
        return RF;
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.rewards_fragment, parent, false);

        return root;
    }
}
