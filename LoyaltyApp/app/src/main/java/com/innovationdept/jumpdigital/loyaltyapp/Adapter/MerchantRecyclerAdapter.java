package com.innovationdept.jumpdigital.loyaltyapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innovationdept.jumpdigital.loyaltyapp.Holder.MerchantRecyclerHolder;
import com.innovationdept.jumpdigital.loyaltyapp.Model.Merchants;
import com.innovationdept.jumpdigital.loyaltyapp.R;

import java.util.List;

/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class MerchantRecyclerAdapter extends RecyclerView.Adapter<MerchantRecyclerHolder> {

    private List<Merchants> itemsList;
    private Context context;

    public MerchantRecyclerAdapter(Context context, List<Merchants> itemsList) {
        this.itemsList = itemsList;
        this.context = context;
    }

    @Override
    public MerchantRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_merchantview, null);
        MerchantRecyclerHolder rcv = new MerchantRecyclerHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(MerchantRecyclerHolder holder, int position) {
        holder.merchinfo[0].setText(itemsList.get(position).getMerch_name());
        holder.merchinfo[1].setText(itemsList.get(position).getMerch_address());
        holder.merchinfo[2].setText(itemsList.get(position).getMerch_contact_per());
        holder.merchinfo[3].setText(itemsList.get(position).getContact_per_num());
    }
    @Override
    public int getItemCount() {
        return this.itemsList.size();
    }
}
