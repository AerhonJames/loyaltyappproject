package com.innovationdept.jumpdigital.loyaltyapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.innovationdept.jumpdigital.loyaltyapp.Holder.RewardsRecyclerHolder;
import com.innovationdept.jumpdigital.loyaltyapp.Model.Merchants;
import com.innovationdept.jumpdigital.loyaltyapp.Model.Rewards;
import com.innovationdept.jumpdigital.loyaltyapp.R;

import java.util.List;


/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class RewardsRecyclerAdapter extends RecyclerView.Adapter<RewardsRecyclerHolder> {

    private List<Rewards> itemsList;
    private Context context;

    public RewardsRecyclerAdapter(Context context, List<Rewards> itemsList) {
        this.itemsList = itemsList;
        this.context = context;
    }

    @Override
    public RewardsRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_rewardsview, null);
        RewardsRecyclerHolder rcv = new RewardsRecyclerHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RewardsRecyclerHolder holder, int position) {

    }
    @Override
    public int getItemCount() {
        return this.itemsList.size();
    }
}
