package com.innovationdept.jumpdigital.loyaltyapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText infoEdittext [] = new EditText[2];
    private Button loginBtn;
    private TextView signupbtn;
    private int backpress = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }
    public void init(){
        /**EditText Initialize*/
        infoEdittext[0] = (EditText)findViewById(R.id.email_add);
        infoEdittext[1] = (EditText)findViewById(R.id.password);
        /**Button Initialize*/
        loginBtn = (Button)findViewById(R.id.loginbtn);
        signupbtn = (TextView)findViewById(R.id.signupbtn);

        loginBtn.setOnClickListener(this);
        signupbtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == loginBtn){
            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        }else if(v == signupbtn){
            Intent intent = new Intent(LoginActivity.this,RegistrationActivity.class);
            startActivity(intent);
        }
    }
    public void onBackPressed(){
        backpress++;

        if(backpress==2){
            System.exit(0);
        }else {
            Toast.makeText(getApplicationContext(),"Press again to exit",Toast.LENGTH_SHORT).show();
        }
    }
}
