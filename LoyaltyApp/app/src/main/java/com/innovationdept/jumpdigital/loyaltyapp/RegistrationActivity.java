package com.innovationdept.jumpdigital.loyaltyapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.innovationdept.jumpdigital.loyaltyapp.Utilities.Utility;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener{
    private Utility utils = new Utility();
    private EditText infoedittext [] = new EditText[7];
    private Spinner infospinner [] = new Spinner[3];
    private ImageView datepicker;
    private Button registerBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        getSupportActionBar().setTitle("Registration");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);
        init();
    }
    public void init(){
        /**Edittext Intialize*/
        infoedittext[0] = (EditText)findViewById(R.id.email_add);
        infoedittext[1] = (EditText)findViewById(R.id.password);
        infoedittext[2] = (EditText)findViewById(R.id.firstname);
        infoedittext[3] = (EditText)findViewById(R.id.lastname);
        infoedittext[4] = (EditText)findViewById(R.id.address);
        infoedittext[5] = (EditText)findViewById(R.id.birthdate);
        infoedittext[5].setEnabled(false);
        infoedittext[6] = (EditText)findViewById(R.id.mobile_num);
        /**Spinner Initialize*/
        infospinner[0] = (Spinner)findViewById(R.id.spingender);
        infospinner[1] = (Spinner)findViewById(R.id.spinregion);
        infospinner[2] = (Spinner)findViewById(R.id.spincity);
        /**Button Initialize*/
        registerBtn = (Button)findViewById(R.id.registerbtn);
        datepicker = (ImageView)findViewById(R.id.datepickerbtn);
        /**Adding onClickListener*/
        registerBtn.setOnClickListener(this);
        datepicker.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if(v == registerBtn){

        }else if(v == datepicker){
            Log.e("Test","DatePicker");
            utils.setDateTimeField(RegistrationActivity.this,infoedittext[5]);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //Write your logic here
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
