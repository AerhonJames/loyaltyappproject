package com.innovationdept.jumpdigital.loyaltyapp.Model;

/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class Users {
    private int id;
    private String email_add;
    private String first_name;
    private String last_name;
    private String mobile_num;
    private String address;
    private String birthdate;
    private String gender;
    private String region;
    private String city;


    public Users(){

    }

    public Users(int id, String email_add, String first_name, String last_name, String mobile_num, String address, String birthdate, String gender, String region, String city) {
        this.id = id;
        this.email_add = email_add;
        this.first_name = first_name;
        this.last_name = last_name;
        this.mobile_num = mobile_num;
        this.address = address;
        this.birthdate = birthdate;
        this.gender = gender;
        this.region = region;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail_add() {
        return email_add;
    }

    public void setEmail_add(String email_add) {
        this.email_add = email_add;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getMobile_num() {
        return mobile_num;
    }

    public void setMobile_num(String mobile_num) {
        this.mobile_num = mobile_num;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Users{" +
                "id=" + id +
                ", email_add='" + email_add + '\'' +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", mobile_num='" + mobile_num + '\'' +
                ", address='" + address + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", gender='" + gender + '\'' +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}
