package com.innovationdept.jumpdigital.loyaltyapp.Model;

/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class Merchants {

    private int merch_id;
    private String merch_name;
    private String merch_address;
    private String merch_contact_per;
    private String reg_merch_date;
    private String metch_image;
    private String contact_per_num;

    public Merchants(){

    }

    public Merchants(int merch_id, String merch_name, String merch_address, String merch_contact_per, String reg_merch_date, String metch_image, String contact_per_num) {
        this.merch_id = merch_id;
        this.merch_name = merch_name;
        this.merch_address = merch_address;
        this.merch_contact_per = merch_contact_per;
        this.reg_merch_date = reg_merch_date;
        this.metch_image = metch_image;
        this.contact_per_num = contact_per_num;
    }

    public int getMerch_id() {
        return merch_id;
    }

    public void setMerch_id(int merch_id) {
        this.merch_id = merch_id;
    }

    public String getMerch_name() {
        return merch_name;
    }

    public void setMerch_name(String merch_name) {
        this.merch_name = merch_name;
    }

    public String getMerch_address() {
        return merch_address;
    }

    public void setMerch_address(String merch_address) {
        this.merch_address = merch_address;
    }

    public String getMerch_contact_per() {
        return merch_contact_per;
    }

    public void setMerch_contact_per(String merch_contact_per) {
        this.merch_contact_per = merch_contact_per;
    }

    public String getReg_merch_date() {
        return reg_merch_date;
    }

    public void setReg_merch_date(String reg_merch_date) {
        this.reg_merch_date = reg_merch_date;
    }

    public String getMetch_image() {
        return metch_image;
    }

    public void setMetch_image(String metch_image) {
        this.metch_image = metch_image;
    }

    public String getContact_per_num() {
        return contact_per_num;
    }

    public void setContact_per_num(String contact_per_num) {
        this.contact_per_num = contact_per_num;
    }

    @Override
    public String toString() {
        return "Merchants{" +
                "merch_id=" + merch_id +
                ", merch_name='" + merch_name + '\'' +
                ", merch_address='" + merch_address + '\'' +
                ", merch_contact_per='" + merch_contact_per + '\'' +
                ", reg_merch_date='" + reg_merch_date + '\'' +
                ", metch_image='" + metch_image + '\'' +
                ", contact_per_num='" + contact_per_num + '\'' +
                '}';
    }
}
