package com.innovationdept.jumpdigital.loyaltyapp.Model;

/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class Products {

    private int id;
    private String prod_name;
    private String prod_image;
    private int prod_points;
    private int merchant_id;

    public Products(){

    }


    public Products(int id, String prod_name, String prod_image, int prod_points, int merchant_id) {
        this.id = id;
        this.prod_name = prod_name;
        this.prod_image = prod_image;
        this.prod_points = prod_points;
        this.merchant_id = merchant_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getProd_image() {
        return prod_image;
    }

    public void setProd_image(String prod_image) {
        this.prod_image = prod_image;
    }

    public int getProd_points() {
        return prod_points;
    }

    public void setProd_points(int prod_points) {
        this.prod_points = prod_points;
    }

    public int getMerchant_id() {
        return merchant_id;
    }

    public void setMerchant_id(int merchant_id) {
        this.merchant_id = merchant_id;
    }

    @Override
    public String toString() {
        return "Products{" +
                "id=" + id +
                ", prod_name='" + prod_name + '\'' +
                ", prod_image='" + prod_image + '\'' +
                ", points=" + prod_points +
                ", merchant_id=" + merchant_id +
                '}';
    }
}
