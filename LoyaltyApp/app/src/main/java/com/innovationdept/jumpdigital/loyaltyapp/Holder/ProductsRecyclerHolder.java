package com.innovationdept.jumpdigital.loyaltyapp.Holder;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.innovationdept.jumpdigital.loyaltyapp.R;


/**
 * Created by AerhonOliveros on 12/27/2016.
 */

public class ProductsRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView prodinfo[] = new TextView[2];
    public ImageView prod_Image;
    public ProductsRecyclerHolder(View ItemView){
        super(ItemView);
        itemView.setOnClickListener(this);
        prodinfo[0] = (TextView)ItemView.findViewById(R.id.prod_name);
        prodinfo[1] = (TextView)ItemView.findViewById(R.id.prod_points);
        prod_Image = (ImageView)ItemView.findViewById(R.id.prod_image);
    }
    @Override
    public void onClick(View v) {

    }
}
